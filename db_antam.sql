-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2017 at 09:08 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_antam`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_actual`
--

CREATE TABLE `tb_actual` (
  `id_act` int(5) NOT NULL,
  `date_act` date NOT NULL,
  `actual` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_data`
--

CREATE TABLE `tb_data` (
  `id` int(1) NOT NULL,
  `iup` varchar(50) NOT NULL,
  `frontmin` varchar(50) NOT NULL,
  `frontcode` varchar(50) NOT NULL,
  `plan` varchar(50) NOT NULL,
  `actual` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_data`
--

INSERT INTO `tb_data` (`id`, `iup`, `frontmin`, `frontcode`, `plan`, `actual`) VALUES
(12, 'California', 'Debian', 'Mandriva', 'OpenSUSE', 'Jackalope'),
(14, 'Alabama', 'Xenial', 'Xerus', 'Fuji', 'Kodiak'),
(16, 'Sierra', '1', '2', '3', '4'),
(17, 'Mavericks', '6', '7', '8', '9'),
(18, 'Gutsy Gibbon', 'GG', 'WP', 'GG', 'WP'),
(19, 'Artful Aardvark', 'Nama', 'Saya', 'Kamal', 'Haris'),
(20, 'Hoary Hedgehog', '43tr4', '4g554g', '45gg', '4g5g'),
(21, 'Warty Warhog', 'feweewe', 'wfe', 'ewfweff', 'wefewf'),
(22, '', 'dasd', 'ads', 'asd', 'asdsa'),
(23, 'Hoary Hedgehog', 'sds', 'sadas', 'asdasd', 'asdsd'),
(24, 'Warty Warhog', '2', '2', '2', '2');

-- --------------------------------------------------------

--
-- Table structure for table `tb_iup`
--

CREATE TABLE `tb_iup` (
  `id` int(2) NOT NULL,
  `iup` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_iup`
--

INSERT INTO `tb_iup` (`id`, `iup`) VALUES
(1, 'Artful Aardvark'),
(2, 'Warty Warhog'),
(3, 'Hoary Hedgehog'),
(4, 'Dapper Drake'),
(5, 'Edgy Eft'),
(6, 'Breezy Badger'),
(7, 'Feisty Fawn'),
(8, 'Gutsy Gibbon'),
(9, 'Hardy Heron'),
(10, 'Intrepid Ibex'),
(11, 'Jaunty Jackalope');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `uname` varchar(20) NOT NULL,
  `pass` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`uname`, `pass`) VALUES
('tri@nanda.com', 'tri'),
('mal', 'mal');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_actual`
--
ALTER TABLE `tb_actual`
  ADD PRIMARY KEY (`id_act`);

--
-- Indexes for table `tb_data`
--
ALTER TABLE `tb_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_iup`
--
ALTER TABLE `tb_iup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_actual`
--
ALTER TABLE `tb_actual`
  MODIFY `id_act` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_data`
--
ALTER TABLE `tb_data`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tb_iup`
--
ALTER TABLE `tb_iup`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Nickel Mining Database | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="dist/css/skins/skin-antam.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->


    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">

                <!-- Logo -->
                <a href="index2.html" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="dist/img/logoaja.png"></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><img src="dist/img/antam3.png"> </span>
                </a>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    </a>

                    <p class="navbar-text whitetext" >PT Antam (Persero) TBK</p>

                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">

                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="dist/img/user8-128x128.jpg" class="user-image" alt="User Image">
                                    <span class="hidden-xs">Kamal Haris</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="dist/img/user8-128x128.jpg" class="img-circle" alt="User Image">

                                        <p>
                                            Kamal Haris - Web Developer
                                            <small>Member since May. 2017</small>
                                        </p>
                                    </li>

                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="act-logout.php" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>

                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="dist/img/user8-128x128.jpg" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>Kamal Haris</p>
                            <a href="#">Lorem ipsum dolor sit amet</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">NAVIGATION</li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="coba.html"><i class="fa fa-cubes"></i> Home</a></li>
                                <li><a href="#"><i class="fa fa-cubes"></i> Front Mining
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li><a href="frontmin_add.php"><i class="fa fa-circle-o"></i> Add New Entry</a></li>
                                        
                                    </ul>
                                <li><a href="#"><i class="fa fa-cubes"></i> Inpit Drilling</a></li>
                                <li><a href="sampling.html"><i class="fa fa-cubes"></i> Sampling</a></li>
                                <li><a href="#"><i class="fa fa-cubes"></i> Sample Preparation</a></li>
                                <li><a href="#"><i class="fa fa-cubes"></i> Sample Analysis</a></li>
                                <li><a href="#"><i class="fa fa-cubes"></i> Equipment Preparation</a></li>
                                <li><a href="#"><i class="fa fa-cubes"></i> Mining Operations</a></li>  
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-laptop"></i>
                                <span>Account</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> Edit Profile</a></li>
                                <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Logout</a></li>
                            </ul>
                        </li>

                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Nickel Mining Database</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active">Front Mining</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Front Mining - Master Entry</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>IUP</th>
                                        <th>Front Mining</th>
                                        <th>Front Code</th>
                                        <th>Plan (Ha)</th>
                                        <th>Actual (Ha)</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    //iclude file koneksi ke database
                                    include('connect.php');

                                    //query ke database dg SELECT table siswa diurutkan berdasarkan NIS paling besar
                                    $query = mysql_query("SELECT * FROM tb_data") or die(mysql_error());

                                    //cek, apakakah hasil query di atas mendapatkan hasil atau tidak (data kosong atau tidak)
                                    if (mysql_num_rows($query) == 0) { //ini artinya jika data hasil query di atas kosong
                                        //jika data kosong, maka akan menampilkan row kosong
                                        echo '<tr><td colspan="6">Tidak ada data!</td></tr>';
                                    } else { //else ini artinya jika data hasil query ada (data diu database tidak kosong)
                                        //jika data tidak kosong, maka akan melakukan perulangan while
                                        $no = 1; //membuat variabel $no untuk membuat nomor urut
                                        while ($data = mysql_fetch_assoc($query)) { //perulangan while dg membuat variabel $data yang akan mengambil data di database
                                            //menampilkan row dengan data di database
                                            echo '<tr>';
                                            echo '<td>' . $no . '</td>'; //menampilkan nomor urut
                                            echo '<td>' . $data['iup'] . '</td>'; //menampilkan data nis dari database
                                            echo '<td>' . $data['frontmin'] . '</td>'; //menampilkan data nama lengkap dari database
                                            echo '<td>' . $data['frontcode'] . '</td>'; //menampilkan data kelas dari database
                                            echo '<td>' . $data['plan'] . '</td>'; //menampilkan data jurusan dari database
                                            echo '<td>' . $data['actual'] . '</td>'; //menampilkan data jurusan dari database
                                            echo '<td> <button title="Edit" type="button" class="btn btn-default"><i class="fa fa-edit"></i></button>
                                            <button title="Delete" type="button" class="btn btn-default"><i class="fa fa-remove"></i></button></td>';
                                            echo '</tr>';

                                            $no++; //menambah jumlah nomor urut setiap row
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>IUP</th>
                                        <th>Front Mining</th>
                                        <th>Front Code</th>
                                        <th>Plan (Ha)</th>
                                        <th>Actual (Ha)</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                </section>
                <style>
                    .whitetext {
                        color: aliceblue;
                    }

                </style>

                <!-- jQuery 2.2.3 -->
                <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
                <!-- Bootstrap 3.3.6 -->
                <script src="bootstrap/js/bootstrap.min.js"></script>
                <!-- DataTables -->
                <script src="plugins/datatables/jquery.dataTables.min.js"></script>
                <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
                <!-- SlimScroll -->
                <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
                <!-- FastClick -->
                <script src="plugins/fastclick/fastclick.js"></script>
                <!-- AdminLTE App -->
                <script src="dist/js/app.min.js"></script>
                <!-- page script -->
                <script>
                    $('#example2').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "scrollX": true,
                        "autoWidth": false
                    });
                </script>
                </body>
                </html>


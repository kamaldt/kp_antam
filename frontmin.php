

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Nickel Mining Database | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="dist/css/skins/skin-antam.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script type="text/javascript">
            function edt_id(id)
            {
                if (confirm('Sure to edit ?'))
                {
                    window.location.href = 'edit_data.php?edit_id=' + id;
                }
            }
            function delete_id(id)
            {
                if (confirm('Sure to Delete ?'))
                {
                    window.location.href = 'index.php?delete_id=' + id;
                }
            }
        </script>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">

                <!-- Logo -->
                <a href="index2.html" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="dist/img/logoaja.png"></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><img src="dist/img/antam3.png"> </span>
                </a>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    </a>

                    <p class="navbar-text whitetext" >PT Antam (Persero) TBK</p>

                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">

                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="dist/img/user8-128x128.jpg" class="user-image" alt="User Image">
                                    <span class="hidden-xs">Kamal Haris</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="dist/img/user8-128x128.jpg" class="img-circle" alt="User Image">

                                        <p>
                                            Kamal Haris - Web Developer
                                            <small>Member since May. 2017</small>
                                        </p>
                                    </li>

                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="act-logout.php" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>

                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="dist/img/user8-128x128.jpg" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>Kamal Haris</p>
                            <a href="#">Lorem ipsum dolor sit amet</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">NAVIGATION</li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="coba.html"><i class="fa fa-cubes"></i> Home</a></li>
                                <li class="active"><a href="#"><i class="fa fa-cubes"></i> Front Mining</a></li>
                                <li><a href="#"><i class="fa fa-cubes"></i> Inpit Drilling</a></li>
                                <li><a href="sampling.html"><i class="fa fa-cubes"></i> Sampling</a></li>
                                <li><a href="#"><i class="fa fa-cubes"></i> Sample Preparation</a></li>
                                <li><a href="#"><i class="fa fa-cubes"></i> Sample Analysis</a></li>
                                <li><a href="#"><i class="fa fa-cubes"></i> Equipment Preparation</a></li>
                                <li><a href="#"><i class="fa fa-cubes"></i> Mining Operations</a></li>  
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-laptop"></i>
                                <span>Account</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> Edit Profile</a></li>
                                <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Logout</a></li>
                            </ul>
                        </li>

                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Nickel Mining Database</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active">Front Mining</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Front Mining - Master Entry</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <form class="form-horizontal" action="add-process.php" method="post">
                                        <div class="box-body">
                                            <div class="box-body">

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">IUP</label>

                                                    <div class="col-sm-10">
                                                        <select name="iup" class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Alabama</option>
                                                            <option>Alaska</option>
                                                            <option>California</option>
                                                            <option>Delaware</option>
                                                            <option>Tennessee</option>
                                                            <option>Texas</option>
                                                            <option>Washington</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Front Mining</label>

                                                    <div class="col-sm-10">
                                                        <input name="frontmining" type="text" class="form-control" id="inputEmail3" placeholder="Front Mining">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Front Code</label>

                                                    <div class="col-sm-10">
                                                        <input name="frontcode" type="text" class="form-control" id="inputEmail3" placeholder="Front Code">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Plan (Ha)</label>

                                                    <div class="col-sm-10">
                                                        <input name="plan" type="text" class="form-control" id="inputEmail3" placeholder="Plan (Ha)">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Actual (Ha)</label>

                                                    <div class="col-sm-10">
                                                        <input name="actual" type="text" class="form-control" id="inputEmail3" placeholder="Actual (Ha)">
                                                    </div>
                                                </div>

                                            </div>  


                                        </div>
                                </div>

                                <!-- /.form-group -->


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Actual Entry (Ha)</label>

                                        <textarea class="form-control" rows="5" placeholder="Table Here.."></textarea>

                                    </div>
                                    <!-- /.form-group -->

                                    <div class="btn-group">
                                        <a href="frontmin_add.php" type="button" class="btn btn-app">
                                            <i class="fa fa-edit"></i>New</a>
                                        <a href="frontmin_edit.php" type="button" class="btn btn-app">
                                            <i class="fa fa-repeat"></i>Edit</a>
                                        <button type="button" class="btn btn-app">
                                            <i class="fa fa-remove"></i>Delete</button>

                                    </div>
                                    <!-- /.form-group -->
                                </div>

                            </div>
                            </form>
                            <!-- /.col -->

                        </div>


                        <div class="box box-warning">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">

                                        <form class="form-horizontal">
                                            <div class="box-body">
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-2 control-label">Filter Data</label>

                                                        <div class="col-sm-10">
                                                            <select class="form-control select2" style="width: 100%;">
                                                                <option>Alabama</option>
                                                                <option>Alaska</option>
                                                                <option selected="selected">California</option>
                                                                <option>Delaware</option>
                                                                <option>Tennessee</option>
                                                                <option>Texas</option>
                                                                <option>Washington</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputPassword3" class="col-sm-2 control-label">Find Data</label>

                                                        <div class="col-sm-10">
                                                            <select class="form-control select2" style="width: 100%;">
                                                                <option>Alabama</option>
                                                                <option>Alaska</option>
                                                                <option>California</option>
                                                                <option>Delaware</option>
                                                                <option>Tennessee</option>
                                                                <option>Texas</option>
                                                                <option selected="selected">Washington</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="inputPassword3" class="col-sm-2 control-label"> </label>

                                                        <button type="button" class="btn btn-app">
                                                            <i class="fa fa-search"></i>Find</button>

                                                    </div>

                                                </div>
                                                <!-- /.box-footer -->
                                        </form>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Front Mining List Data</label>

                                        <textarea class="form-control" rows="5" placeholder="Table Here.."></textarea>

                                    </div>
                                    <!-- /.form-group -->

                                    <!-- /.form-group -->
                                </div>

                                <!-- /.col -->
                            </div>
                        </div>    
                    </div>


                    <style>
                        .whitetext {
                            color: aliceblue;
                        }

                    </style>

                    <!-- jQuery 2.2.3 -->
                    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
                    <!-- Bootstrap 3.3.6 -->
                    <script src="bootstrap/js/bootstrap.min.js"></script>
                    <!-- FastClick -->
                    <script src="plugins/fastclick/fastclick.js"></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/app.min.js"></script>
                    <!-- Sparkline -->
                    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
                    <!-- jvectormap -->
                    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
                    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
                    <!-- SlimScroll 1.3.0 -->
                    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
                    <!-- ChartJS 1.0.1 -->
                    <script src="plugins/chartjs/Chart.min.js"></script>
                    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
                    <script src="dist/js/pages/dashboard2.js"></script>
                    <!-- AdminLTE for demo purposes -->
                    <script src="dist/js/demo.js"></script>
                    </body>
                    </html>

